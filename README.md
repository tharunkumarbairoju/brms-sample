Red Hat JBoss BRMS use a centralized repository where all resources are stored. This ensures consistency, transparency, and the ability to audit across the business. Business users can modify business logic and business processes without requiring assistance from IT personnel.

1.1 Need of BRMS.
To increase the Line of Business (LOB) control over implemented decision logic for compliance and business management.
To express decision logic with increased precision, using business vocabulary or graphical rule representations.
To reduce or remove the reliance on IT for changes in the production systems.
To improve process efficiency.
